def migrate_contributors(source):
        if 'creators' in source:
            if 'contributors_edt_aut' not in source:
                source['contributors_edt_aut'] = source['creators']
                for contributor in source['contributors_edt_aut']:
                    contributor['type'] = 'http://www.loc.gov/loc.terms/relators/AUT'
                    del contributor['name']['lineage']
                    del contributor['name']['honourific']
            elif len(source['creators']) != len(source['contributors_edt_aut']):
                for creator in source['creators']:
                    already_exists = False
                    for contributor in source['contributors_edt_aut']:
                        if creator['name']['family'] == contributor['name']['family']:
                            already_exists = True
                    if not already_exists:
                        new_creator = {
                            'type': 'http://www.loc.gov/loc.terms/relators/AUT',
                            'name': {
                                'family': creator['name']['family'],
                                'given': creator['name']['given'],
                            }
                        }
                        source['contributors_edt_aut'].append(new_creator)
            else:
                pass

        if 'advisors' in source:
            if 'contributors_ths_dgs' not in source:
                source['contributors_ths_dgs'] = source['advisors']
                for contributor in source['contributors_ths_dgs']:
                    del contributor['name']['lineage']
                    del contributor['name']['honourific']
                    contributor['type'] = 'http://www.loc.gov/loc.terms/relators/THS'
            else:
                for advisor in source['advisors']:
                    already_exists = False
                    for contributor in source['contributors_ths_dgs']:
                        if advisor['name']['family'] == contributor['name']['family']:
                            already_exists = True
                    if not already_exists:
                        new_advisor = {
                            'type': 'http://www.loc.gov/loc.terms/relators/THS',
                            'name': {
                                'family': advisor['name']['family'],
                                'given': advisor['name']['given'],
                            }
                        }
                        source['contributors_ths_dgs'].append(new_advisor)

        if 'committee_members' in source:
            if 'contributors_ths_dgs' not in source:
                source['contributors_ths_dgs'] = source['committee_members']
                for contributor in source['contributors_ths_dgs']:
                    del contributor['name']['lineage']
                    del contributor['name']['honourific']
                    contributor['type'] = 'http://www.loc.gov/loc.terms/relators/DGS'
            else:
                for committee_members in source['committee_members']:
                    already_exists = False
                    for contributor in source['contributors_ths_dgs']:
                        if committee_members['name']['family'] == contributor['name']['family']:
                            already_exists = True
                    if not already_exists:
                        new_committee_remember = {
                            'type': 'http://www.loc.gov/loc.terms/relators/THS',
                            'name': {
                                'family': committee_members['name']['family'],
                                'given': committee_members['name']['given'],
                            }
                        }
                        source['contributors_ths_dgs'].append(new_committee_remember)