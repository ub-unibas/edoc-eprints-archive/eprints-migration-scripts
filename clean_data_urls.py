import csv
import logging
import sys

import requests
from elasticsearch6 import Elasticsearch
from elasticsearch6.helpers import scan
from requests import ConnectTimeout

collected_urls = list()


def check_urls(related_urls, processed):
    logging.info("Found " + str(len(related_urls)) + " related urls. Checking for validity")
    with open('output/http_errors.csv', 'a') as fp:
        csv_fp_writer = csv.writer(fp)
        with open('output/forbidden.csv', 'a') as forbidden_file:
            csv_forbidden_file = csv.writer(forbidden_file)
            with open('output/other_bad_status.csv', 'a') as urls_file:
                csv_urls_file = csv.writer(urls_file)
                for related_url_item in related_urls:
                    try:
                        print(related_url_item)
                        response = requests.get(related_url_item['url'])
                        if response.status_code < 400:
                            pass
                        elif response.status_code == 403:
                            csv_forbidden_file.writerow(
                                [related_url_item['id'], related_url_item['url'], response.status_code,
                                 related_url_item['uri']])
                        elif response.status_code >= 400:
                            csv_urls_file.writerow(
                                [related_url_item['id'], related_url_item['url'], response.status_code,
                                 related_url_item['uri']])
                    except ConnectTimeout:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "TimeoutError",
                                 related_url_item['uri']])
                    except requests.exceptions.SSLError:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "SSLException",
                                 related_url_item['uri']])
                    except requests.exceptions.ConnectionError:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "ConnectionError",
                                 related_url_item['uri']])
                    except requests.exceptions.MissingSchema:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "MissingSchema",
                                 related_url_item['uri']])
                    except requests.exceptions.ReadTimeout:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "ReadTimeout",
                                 related_url_item['uri']])
                    except requests.exceptions.InvalidSchema:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], "InvalidSchema",
                                 related_url_item['uri']])
                    except Exception as e:
                        csv_fp_writer.writerow(
                                [related_url_item['id'], related_url_item['url'], str(e),
                                 related_url_item['uri']])
                    processed += 1
                    logging.info(f"Processed {processed} / {len(related_urls)} urls.")

                    with open('current_count.txt', 'w') as x:
                        x.write(str(processed))


def clean_related_urls(related_urls: list, eprintid: str, uri: str) -> list:
    new_related_urls = list()
    for item in related_urls:
        if 'url' not in item:
            # any element that doesn't have an url is not copied.
            continue

        if item['url'] is None:
            # in some cases the url value is null.
            continue

        if item['url'].startswith('http://sfx.'):
            # remove old sfx links as they are no longer needed
            continue

        if item['url'] == '':
            # remove empty urls.
            continue

        collected_urls.append({
            'url': item['url'].strip(),
            'id': eprintid,
            'uri': uri
        })

        if 'type' in item and item['type'] == 'pub':
            item['type'] = 'doc'

        new_related_urls.append(item)

    return new_related_urls


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    client = Elasticsearch('http://localhost:9200')
    logging.info("Collecting data from edoc-vmware.")
    for doc in scan(client, index="edoc-vmware"):
        source = doc["_source"]
        if 'related_url' in source:
            related_url = source['related_url']
            cleaned_related_urls = clean_related_urls(related_url, source['eprintid'], source['uri'])
            source['related_url'] = cleaned_related_urls

    logging.info("Start checking urls.")
    with open('current_count.txt', 'r') as x:
        processed_count = x.read()

    sorted_urls = sorted(collected_urls, key=lambda s: s['uri'])

    check_urls(sorted_urls[int(processed_count):], int(processed_count))
