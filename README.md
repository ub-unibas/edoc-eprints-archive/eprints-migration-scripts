# Eprints Migration Scripts

A collection of scripts used to prepare the data for the UNIverse / DSpace migration.

## Run

```shell
docker run -v <dir>:/app/output cr.gitlab.switch.ch/ub-unibas/edoc-eprints-archive/eprints-migration-scripts:latest
```

## ISBN Validation
The validation:

- https://pypi.org/project/isbnlib/