import csv
import logging
import sys
from typing import List, Tuple

import isbnlib
from elasticsearch6 import Elasticsearch
from elasticsearch6.helpers import scan

no_valid_isbn_list: List[List[str]] = list()
multiple_isbn_list: List[List[str]] = list()
invalid_isbn_list: List[List[str]] = list()


def clean_isbn(param: str, uri: str) -> Tuple[List[str], List[str], List[str], List[List[str]]]:
    isbn = param.strip()
    isbn = isbn.replace('–', '-')
    isbn = isbn.replace('­-', '-')
    isbn = isbn.replace('‑', '-')
    isbn = isbn.replace('-­', '-')
    isbns = isbnlib.get_isbnlike(isbn, level='normal')
    multiple_isbn_text = []
    invalid_isbn_text = list()
    if len(isbns) == 0:
        return [], [uri, isbn], [], []

    if len(isbns) > 1:
        multiple_isbn_text = [uri, isbn]

    new_isbn_list = set()
    for isbn in isbns:
        if isbnlib.is_isbn10(isbn):
            isbn = isbnlib.to_isbn13(isbn)

        if isbnlib.is_isbn13(isbn):
            value = isbnlib.mask(isbn)
        else:
            value = ''
        if value == '':
            logging.warning('Invalid ISBN: {}'.format(isbn))
            invalid_isbn_text.append([uri, isbn])
        else:
            new_isbn_list.add(value)

    return list(new_isbn_list), [], multiple_isbn_text, invalid_isbn_text


def update_isbn(source_doc, tag):
    new_values, no_valid_isbn, multiple_isbn, invalid_isbns = clean_isbn(str(source_doc[tag]), source_doc['uri'])
    if len(no_valid_isbn) == 2:
        no_valid_isbn_list.append(no_valid_isbn)
    if len(multiple_isbn) == 2:
        multiple_isbn_list.append(multiple_isbn)

    for i in invalid_isbns:
        invalid_isbn_list.append(i)

    if len(new_values) > 0:
        source_doc[tag] = ';'.join(new_values)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    client = Elasticsearch("http://localhost:9200")
    for doc in scan(client, index="edoc-vmware"):
        source = doc["_source"]

        if source['eprint_status'] == 'buffer':
            continue

        if 'isbn' in source:
            update_isbn(source, 'isbn')

        if 'isbn_e' in source:
            update_isbn(source, 'isbn_e')

    with open('output/no_isbn_like_found.csv', 'w') as no_isbn_file_fp:
        writer = csv.writer(no_isbn_file_fp)
        sorted_values = sorted(no_valid_isbn_list, key=lambda x: x[0])
        writer.writerows(sorted_values)

    with open('output/multiple_isbn.csv', 'w') as multiple_isbn_fp:
        writer_2 = csv.writer(multiple_isbn_fp)
        sorted_values = sorted(multiple_isbn_list, key=lambda x: x[0])
        writer_2.writerows(sorted_values)

    with open('output/invalid_isbn.csv', 'w') as invalid_isbn_fp:
        writer_3 = csv.writer(invalid_isbn_fp)
        sorted_values = sorted(invalid_isbn_list, key=lambda x: x[0])
        writer_3.writerows(sorted_values)

