import json
import logging
import re
import sys

from elasticsearch6 import Elasticsearch
from elasticsearch6.helpers import scan, bulk

publication_types = [
    'Book item',
    'Edited book',
    'Authored book',
    'Journal article',
    'Journal item',
    'Conference paper',
    'News item emission',
    'Discussion paper / Internet publication',
    'News item print',
    'Other publications',
    'Internet publication'
]

filter_value = '|'.join(publication_types)
filter_regex = re.compile(r'^Publication type according to Uni Basel Research Database: (' + filter_value + r')$')

new_index = '00-edoc-vmware-notes-2024-06-17'


def generator(documents):
    for d in documents:
        d['_op_type'] = 'index'
        d['_index'] = new_index
        d['_type'] = '_doc'
        yield d


def clean_notes(note: str) -> str:
    note: str = note.strip('- ')
    if 'Publication type according to Uni Basel Research Database' in note:
        if filter_regex.match(note):
            return ""
        else:
            new_notes = list()
            notes = note.split(' -- ')
            for n in notes:
                n = n.strip()
                if filter_regex.match(n):
                    # don't add fdb note.
                    pass
                else:
                    new_notes.append(n.strip())

            if len(new_notes) > 0:
                return ' -- '.join(new_notes)
            else:
                return ""


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    client = Elasticsearch("http://localhost:9200")
    count = 0
    changed_notes = 0
    update_documents = list()
    for doc in scan(client, index="edoc-vmware"):
        source = doc["_source"]

        if 'note' in source:
            new_note = clean_notes(source['note'])
            if new_note == "":
                del source['note']
            else:
                source['note'] = new_note

    print("deleted notes: ", count)
    print("updated notes: ", changed_notes)
    print("total notes: ", count + changed_notes)

    client.indices.delete(index=new_index)
    with open('mapping.json', 'r') as mapping_file:
        client.indices.create(index=new_index, body=json.load(mapping_file))
    bulk(client, generator(update_documents))
