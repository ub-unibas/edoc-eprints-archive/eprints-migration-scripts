FROM python:3-slim

WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt
ENTRYPOINT [ "python", "/app/clean_data.py", "--host", "http://ub-metrics20.ub.unibas.ch:9200" ]