import argparse
import json
import logging
import sys
from datetime import datetime

import elasticsearch7.exceptions
from elasticsearch7 import Elasticsearch
from elasticsearch7.helpers import scan, bulk

from check_contributors import migrate_contributors
from clean_data_isbn import update_isbn
from clean_data_note import clean_notes
from clean_data_urls import clean_related_urls

current_date = datetime.now().strftime("%Y-%m-%d")

parser = argparse.ArgumentParser(
    prog='edoc-data-migration',
    description='a small toolset to migrate / normalize edoc data'
)

parser.add_argument('-i', '--index', help='target elasticsearch index.',
                    default=f'00-edoc-vmware-enriched-{current_date}')
parser.add_argument('--host', help='elasticsearch host', default='http://localhost:9200')
args = parser.parse_args()

new_index = args.index
host = args.host
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

logging.getLogger('requests').setLevel(logging.INFO)
logging.getLogger('urllib3').setLevel(logging.INFO)
logging.getLogger('elasticsearch').setLevel(logging.INFO)

logging.info("Connecting to Elasticsearch at host {}.".format(host))


def generator(gen_docs):
    for d in gen_docs:
        d['_op_type'] = 'index'
        d['_index'] = new_index
        d['_type'] = '_doc'
        yield d


if __name__ == '__main__':
    client = Elasticsearch(host)

    documents = list()
    for document in scan(client, index="edoc-vmware"):
        source = document['_source']

        if source['eprint_status'] == 'buffer':
            continue

        if 'isbn' in source:
            update_isbn(source, 'isbn')

        if 'isbn_e' in source:
            update_isbn(source, 'isbn_e')

        if 'note' in source:
            new_note = clean_notes(source['note'])
            if new_note == "":
                del source['note']
            else:
                source['note'] = new_note

        if 'related_url' in source:
            related_url = source['related_url']
            cleaned_related_urls = clean_related_urls(related_url, source['eprintid'], source['uri'])
            source['related_url'] = cleaned_related_urls

        if 'mcss_id' in source:
            source['mcss_id'] = source['mcss_id'].strip()

        if 'universe_id' in source:
            source['universe_id'] = source['universe_id'].strip()

        migrate_contributors(source)

        documents.append(document)

    if client.indices.exists(index=new_index):
        client.indices.delete(index=new_index)
    with open('mapping.json', 'r') as mapping_file:
        client.indices.create(index=new_index, body=json.load(mapping_file))
    try:
        client.indices.delete_alias(index='00-edoc-vmware-enriched-*', name='edoc-vmware-enriched')
    except elasticsearch7.exceptions.NotFoundError:
        pass
    client.indices.put_alias(index=new_index, name='edoc-vmware-enriched')
    bulk(client, generator(documents))
